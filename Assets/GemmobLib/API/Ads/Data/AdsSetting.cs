﻿using Gemmob.Common.Data;
using System;
using UnityEngine;

namespace Gemmob.API.Ads {
    public class AdsSetting {
        #region Admob Test ID
        private static readonly string AndroidAppId = "ca-app-pub-3940256099942544~3347511713";
        private static readonly string IOSAppId = "ca-app-pub-3940256099942544~1458002511";

        private static readonly string BannerAndroidAdUnitId = "ca-app-pub-3940256099942544/6300978111";
        private static readonly string BannerIOSAdUnitId = "ca-app-pub-3940256099942544/2934735716";

        private static readonly string InterstitialAndroidAdUnitId = "ca-app-pub-3940256099942544/1033173712";
        private static readonly string InterstitialIOSAdUnitId = "ca-app-pub-3940256099942544/4411468910";

        private static readonly string RewardedAndroidAdUnitId = "ca-app-pub-3940256099942544/5224354917";
        private static readonly string RewardedIOSAdUnitId = "ca-app-pub-3940256099942544/1712485313";
        #endregion

        #region Backup Test Id
        private static readonly string StartappTestId = "test";//"200689009"; // Over1 Free
        private static readonly string UnityTestId = Config.IsAndroid ? "3338775" : "3338774";
        #endregion

        public const string IosConfigFileName = "GemmobAdsIosConfig";
        public const string AndroidConfigFileName = "GemmobAdsAndroidConfig";

        private const string AdsConfigKey = "GemmobAds";
        private const string ConfigFileFolder = "Assets/Resources/";

        public static AdsConfig.AdmobInfo AndroidAdmobTestInfo {
            get {
                return new AdsConfig.AdmobInfo(AndroidAppId, BannerAndroidAdUnitId, InterstitialAndroidAdUnitId, RewardedAndroidAdUnitId);
            }
        }

        public static AdsConfig.AdmobInfo IosAdmobTestInfo {
            get {
                return new AdsConfig.AdmobInfo(IOSAppId, BannerIOSAdUnitId, InterstitialIOSAdUnitId, RewardedIOSAdUnitId);
            }
        }

        public static AdsConfig.BackupInfo BackupTestInfo {
            get => new AdsConfig.BackupInfo() { startapp = StartappTestId, unityads = UnityTestId };
        }

        public static AdsConfig.ApiInfo LoadIosConfigFromResouceFolder() {
            return LoadConfigFromResouceFolder(IosConfigFileName);
        }

        public static AdsConfig.ApiInfo LoadAndroidConfigFromResouceFolder() {
            return LoadConfigFromResouceFolder(AndroidConfigFileName);
        }

        public static AdsConfig.ApiInfo LoadConfigFromResouceFolder(string fileName) {
            try {
                Debug.Log(fileName);
                var textAsset = Resources.Load<TextAsset>(fileName);
                var decryptString = Encryption.DecryptString(textAsset.text);
                var config = JsonUtility.FromJson<AdsConfig.ApiInfo>(decryptString);
                Logs.LogFormat("Load Config from json file {0}", fileName);
                return config;
            }
            catch (FormatException) {
                Logs.LogError("Invalid encrypt key, Please download again ads config");
                return null;
            }
            catch (Exception e) {
                Logs.LogErrorFormat("Can not load resource file {0} - {1}", fileName, e.Message);
                return null;
            }
        }

        public static AdsConfig.ApiInfo LoadLocalApiInfo() {
            try {
                var jsonText = SecurePlayerPrefs.GetString(AdsConfigKey);
                if (!string.IsNullOrEmpty(jsonText)) {
                    var config = JsonUtility.FromJson<AdsConfig.ApiInfo>(jsonText);
                    Logs.Log("Load Config from PlayerPrefs");
                    return config;
                }
                else {
                    var config = LoadConfigFromResouceFolder(GetConfigFileName());
                    SaveToLocalConfig(config);
                    return config;
                }
            }
            catch (Exception e) {
                Logs.LogErrorFormat("Can not load config from PlayerPrefs:  {0}", e.Message);
            }

            return LoadConfigFromResouceFolder(GetConfigFileName());
        }

        public static AdsConfig.AdmobInfo LoadLocalAdmobInfo() {

            if (GemmobAdsConfig.Instance.enableTest) {

#if UNITY_ANDROID
                return AndroidAdmobTestInfo;
#endif

#if UNITY_IOS
            return IosAdmobTestInfo;
#endif

            }

            return LoadLocalApiInfo().ads.admob;
        }

        public static AdsConfig.BackupInfo LoadLocalBackupInfo() {
            if (GemmobAdsConfig.Instance.enableTest) {
                return BackupTestInfo;
            }

            return LoadLocalApiInfo().ads.backup;
        }

        private static string GetConfigFileName() {
#if UNITY_ANDROID
            return AndroidConfigFileName;
#endif

#if UNITY_IOS
        return IosConfigFileName;
#endif
        }

        private static void SaveToLocalConfig(AdsConfig.ApiInfo config) {
            try {
                SecurePlayerPrefs.SetString(AdsConfigKey, JsonUtility.ToJson(config));
            }
            catch (Exception e) {
                Logs.LogErrorFormat("Error when save ads config to local {0}", e.Message);
            }
        }

    }
}