﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class AdsTest : MonoBehaviour
{
    private void Start() {
        IngameConsole.Instance.Show();
        Mediation.Instance.Preload();
    }

    public void ShowInterstital() {
        Logs.Log("OnClick ShowInterstital");
        Mediation.Instance.ShowInterstitial("test", () => {
            Logs.Log("OnCompleted Admob_Interstitial");
        }, () => {
            Logs.Log("OnFailed Admob_Interstitial");
        });
    }

    public void ShowRewardVideo() {
        Logs.Log("OnClick ShowRewardVideo");
        Mediation.Instance.ShowRewardVideo("test", () => {
            Logs.Log("OnCompleted Admob_RewardVideo");
        }, () => {
            Logs.Log("OnFailed Admob_RewardVideo");
        });
    }

    public void ShowBannerBottom() {
        Logs.Log("OnClick ShowBannerBottom");
        Mediation.Instance.ShowBannerBottom("test");
    }

    public void ShowBannerTop() {
        Logs.Log("OnClick ShowBannerTop");
        Mediation.Instance.ShowBannerTop("test");
    }

    public void ShowInterstitalUnity() {
        Logs.Log("OnClick ShowInterstitalUnity");
        Mediation.Instance.ShowInterstitialUnity("test", () => {
            Logs.Log("OnCompleted Unity_Interstitial");
        }, () => {
            Logs.Log("OnFailed Unity_Interstitial");
        });
    }

    public void ShowRewardVideoUnity() {
        Logs.Log("OnClick ShowRewardVideoUnity");
        Mediation.Instance.ShowRewardVideoUnity("test", () => {
            Logs.Log("OnCompleted Unity_RewardVideo");
        }, () => {
            Logs.Log("OnFailed Unity_RewardVideo");
        });
    }

    private void OnGUI() {
#if UNITY_EDITOR
        Vector2 center = UnityEditor.Handles.GetMainGameViewSize() / 2;
#else
        Vector2 center = new Vector2(Screen.width / 2, Screen.height / 2);
#endif
        float w = Screen.width / 2;
        float h = w / 4;

        GUILayout.BeginVertical(); 
        if (GUILayout.Button("Show Interstitial", GUILayout.Width(w), GUILayout.Height(h))) {
            ShowInterstital();
        }

        if (GUILayout.Button("Show RewardVideo", GUILayout.Width(w), GUILayout.Height(h))) {
            ShowRewardVideo();
        }

        if (GUILayout.Button("Show Banner Bottom", GUILayout.Width(w), GUILayout.Height(h))) {
            ShowBannerBottom();
        }

        if (GUILayout.Button("Show Banner Top", GUILayout.Width(w), GUILayout.Height(h))) {
            ShowBannerTop();
        }

        GUILayout.Space(20);
        if (GUILayout.Button("Show Interstitial Unity", GUILayout.Width(w), GUILayout.Height(h))) {
            ShowInterstitalUnity();
        }

        if (GUILayout.Button("Show RewardVideo Unity", GUILayout.Width(w), GUILayout.Height(h))) {
            ShowRewardVideoUnity();
        }
        GUILayout.EndVertical();
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(AdsTest))]
public class AdsTestEditor : Editor {
    public override void OnInspectorGUI() {
        if (!Application.isPlaying) return;

        DrawDefaultInspector();

        var test = target as AdsTest;
        if (GUILayout.Button("Show Fake Interstial")) {
            test.ShowInterstital();
        }

        if (GUILayout.Button("Show Fake Reward Video")) {
            test.ShowRewardVideo();
        }

        if (GUILayout.Button("Show Fake Banner Bottom")) {
            test.ShowBannerBottom();
        }

        if (GUILayout.Button("Show Fake Banner Top")) {
            test.ShowBannerTop();
        }
    }

}
#endif