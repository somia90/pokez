﻿using UnityEngine;
using Gemmob.API.Ads;
using UnityEngine.UI;

public class AdsFake : MonoBehaviour {
    [SerializeField] private Text title;
    [SerializeField] private Button btnSuccess, btnLoadFailed;

    System.Action successCallback, failedCallback;
    
    public static void Show(Ad ad, System.Action onSuccessCallback, System.Action onFailedCallback = null) {
        Vector2 res;
#if UNITY_EDITOR
        res = UnityEditor.Handles.GetMainGameViewSize();
#else
        res = new Vector2(Screen.width, Screen.height);
#endif
        GameObject prefab = Resources.Load<GameObject>("AdsFake/AdsFake");
        if (prefab == null) {
            Logs.LogError("[AdsFake] Can't find AdsFake prefab from Resources");
            if (onFailedCallback != null) onFailedCallback.Invoke();
            return;
        }

        res = (res.x < res.y) ? new Vector2(720, 1280) : new Vector2(1280, 720);
        AdsFake ads = Instantiate(prefab).GetComponent<AdsFake>();
        Logs.LogFormat("[ADS] Show Fake resolution: {0}x{1}", res.x, res.y);
        ads.GetComponent<CanvasScaler>().referenceResolution = res;
        ads.Init(res.x < res.y, ad, onSuccessCallback, onFailedCallback);
    }

    public void Init(bool portrait, Ad ad, System.Action onSuccessCallback, System.Action onFailedCallback = null) {
        this.successCallback = onSuccessCallback;
        this.failedCallback = onFailedCallback;

        var body = title.transform.parent as RectTransform;
        var layout = body.GetComponent<VerticalLayoutGroup>();
        if (layout && (!portrait || ad.type == Ad.Type.BannerTop || ad.type == Ad.Type.BannerBottom)) {
            layout.childControlHeight = true;
            layout.childForceExpandHeight = true;
        }

        AddButtonListener(btnSuccess, OnClickSuccessButton);

        if (ad.type == Ad.Type.Interstitial) {
            btnLoadFailed.gameObject.SetActive(false);

            title.text = "Interstitial Fake";
            var text = btnSuccess.GetComponentInChildren<Text>();
            if (text != null) text.text = "OK";
        }
        else if (ad.type == Ad.Type.Rewarded) {
            AddButtonListener(btnLoadFailed, OnClickShowFailedButton);

            title.text = "Video Rewarded Fake";
            var text = btnSuccess.GetComponentInChildren<Text>();
            if (text != null) text.text = "Watch Done";
        }
        else {
            btnLoadFailed.gameObject.SetActive(false);

#if UNITY_EDITOR
            Vector2 screenSize = UnityEditor.Handles.GetMainGameViewSize();
#else
            Vector2 screenSize = new Vector2(Screen.width, Screen.height);
#endif
            var smartheight = (ad as BannerAdmob).GetHeight();// screenSize.y > 720 ? 90 : screenSize.y > 400 ? 50 : 32;

            title.gameObject.SetActive(false);

            var text = btnSuccess.GetComponentInChildren<Text>();
            if (text != null) {
                text.fontSize /= 2;
                text.text = string.Format("This is a banner {0}x{1}\nClick me!", screenSize.x, smartheight);
            }

            body.sizeDelta = new Vector2(screenSize.x, smartheight);

#if UNITY_ANDROID
            int pivot = (ad as BannerAdmob).adPosition == GoogleMobileAds.Api.AdPosition.Top ? 1 : 0;
#else
            int pivot = 0;
#endif
            body.anchorMin = new Vector2(0, pivot);
            body.anchorMax = new Vector2(1, pivot);
            body.pivot = new Vector2(0.5f, pivot);
            body.anchoredPosition = new Vector2(0, 0);
        }
    }

    private void AddButtonListener(Button btn, UnityEngine.Events.UnityAction action) {
        btn.gameObject.SetActive(true);
        btn.onClick.RemoveAllListeners();
        btn.onClick.AddListener(action);
    }

    private void OnClickSuccessButton() {
        if (successCallback != null) successCallback.Invoke();
        Destroy(gameObject);
    }
    
    private void OnClickShowFailedButton() {
        if (failedCallback != null) failedCallback.Invoke();
        Destroy(gameObject);
    }
}
