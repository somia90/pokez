﻿using UnityEngine;
using UniRx;
using System;

namespace Gemmob.API.Ads {
    /**<summary>Current API: AdsMediation v1.1.0 
     * <para>Link: https://it.gemmob.com/gemmob/gemmoblib/tree/master/GemmobPackages/Ads </para>
     * </summary>*/
    public abstract class Ad {
        public enum Sponsor { Admob, Unity }
        public enum Type { Rewarded, Interstitial, BannerBottom, BannerTop }
        public Type type;
        public abstract Sponsor sponsor { get; }
        public string AdName { get; private set; }

        protected string AdUnitId;

        protected Action OnComplete;
        protected Action OnFail;

        protected bool requesting;

        private readonly double[] retryTimes = { 2, 5, 10, 20, 60, 60, 120, 120, 240, 240, 400, 400, 600 };
        protected int retry;

        public string LogPrefix;// = "<color=yellow>[ADS {0}]</color> ";

        protected double GetRetryTime(int retry) {
            return retry < retryTimes.Length && retry >= 0 ? retryTimes[retry] : retryTimes[retryTimes.Length - 1];
        }

        public Ad(string AdUnitId, Type type) {
            this.AdUnitId = AdUnitId;
            this.type = type;
            this.AdName = string.Format("{0}_{1}", sponsor.ToString(), type.ToString());
            LogPrefix = string.Format("<color=yellow>[ADS {0}]</color>", AdName);
        }

        public void Show(Action complete, Action fail) {
            this.OnComplete = complete;
            this.OnFail = fail;
            OnShow();
        }

        public void Request() {
            if (requesting) return;
            if (!Network.IsInternetAvaiable) {
                Logs.LogFormat("{0} Request failed: No internet available.", LogPrefix);
                return;
            }

            requesting = true;
            var delayRequest = GetRetryTime(retry);
            Logs.LogFormat("{0} Request: delay={1}s, retry={2}", LogPrefix, delayRequest, retry);

            Scheduler.MainThreadIgnoreTimeScale.Schedule(TimeSpan.FromSeconds(delayRequest), action => {
                try {
                    OnRequest();
                }
                catch (Exception e) {
                    requesting = false;
                    Logs.LogErrorFormat("{0} Error OnRequest: {1}", LogPrefix, e.Message);
                }
            });
        }
        
        public virtual void Hide() { }

        #region Abstract
        public abstract void Initialize();

        protected abstract void OnRequest();

        protected abstract void OnShow();

        public abstract bool IsLoaded { get; }
        #endregion

        #region Callback
        private void CallSuccess(float delayTime = 0) {
            Callback.CallSchedule(OnComplete, delayTime);
            OnComplete = null;
            OnFail = null;
        }

        protected virtual void OnLoadedSuccessfully() {
            requesting = false;
            retry = 0;
            Logs.LogFormat("{0} LoadedSuccessfully. Ready to show", LogPrefix);
        }

        protected virtual void OnLoadFailed(string errorMsg) {
            Logs.LogErrorFormat("{0} FailedToLoad: {1}", LogPrefix, errorMsg);
            requesting = false;
            retry++;
            Callback.CallSchedule(OnFail);
            Request();
        }

        protected virtual void OnShowFailed(string errorMsg) {
            Logs.LogErrorFormat("{0} FailedToShow: {1}", LogPrefix, errorMsg);
            requesting = false;
            retry++;
            Callback.CallSchedule(OnFail);
            Request();
        }

        protected virtual void OnOpening() {
            Logs.LogFormat("{0} Opening..", LogPrefix);
        }

        protected virtual void OnStartShow() {
            Logs.LogFormat("{0} Start Show", LogPrefix);
        }

        protected virtual void OnClicked() {
            Logs.LogFormat("{0} OnClicked", LogPrefix);
        }

        protected virtual void OnClose(bool completed) {
            Logs.LogFormat("{0} Closed completed={1}. Call Request new one.", LogPrefix, completed);
            Callback.CallSchedule(completed ? OnComplete : OnFail);
            Request();
        }
        #endregion Callback

        #region Handle
        protected virtual void OnAdLoaded() {
            OnLoadedSuccessfully();
        }

        protected virtual void OnAdFailedToLoad(string msg) {
            OnLoadFailed(msg);
        }

        protected virtual void OnAdFailedToShow(string msg) {
            OnShowFailed(msg);
        }

        protected virtual void OnAdOpening() {
            OnOpening();
        }

        protected virtual void OnAdStarted() {
            OnStartShow();
        }

        protected virtual void OnAdClosed() {
            OnClose(true);
        }

        public virtual void OnAdLeavingApplication() {
            OnClicked();
        }
        #endregion Handle
    }
}
