﻿
using UnityEngine.Advertisements;

namespace Gemmob.API.Ads {
    public class RewardedUnity : AdUnity {
        protected override string PlacementID => "rewardedVideo";

        public RewardedUnity(string AdUnitId, Type type) : base(AdUnitId, type) {
            Request();
        }
        
        public override void Initialize() {
            Advertisement.AddListener(this);
        }

    }
}