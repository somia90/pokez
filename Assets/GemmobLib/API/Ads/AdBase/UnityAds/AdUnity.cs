﻿
using UnityEngine.Advertisements;

namespace Gemmob.API.Ads {
    /**<summary>Current SDK: Unity Monetization 3.3.0
   * <para>Link: https://assetstore.unity.com/packages/add-ons/services/unity-monetization-3-3-0-66123 </para>
   * </summary>*/
    public abstract class AdUnity : Ad, IUnityAdsListener {// : IAds {
        private static bool initialized; // TODO: Init only one times althought has multi ads extend this.
        protected abstract string PlacementID { get; }
        public override Sponsor sponsor => Sponsor.Unity;

        public AdUnity(string AdUnitId, Type type) : base(AdUnitId, type) {
            if (!initialized && !Advertisement.isInitialized) {
                initialized = true;
                Advertisement.Initialize(this.AdUnitId, Config.IsDebug, true);
            }
            Initialize();
        }

        public override bool IsLoaded {
            get {
                if (Advertisement.IsReady(PlacementID)) return true;
                Request();
                return false;
            }
        }

        protected override void OnRequest() {
            Advertisement.Load(PlacementID);
        }

        protected override void OnShow() {
            if (IsLoaded) Advertisement.Show(PlacementID);
        }

        #region Callback
        public virtual void OnUnityAdsReady(string placementId) {
            if (!placementId.Equals(this.PlacementID)) return;
            OnAdLoaded();
        }

        public virtual void OnUnityAdsDidError(string message) {
            OnAdFailedToLoad(message);
        }

        public virtual void OnUnityAdsDidStart(string placementId) {
            if (!placementId.Equals(this.PlacementID)) return;
            OnAdStarted();
        }

        public virtual void OnUnityAdsDidFinish(string placementId, ShowResult showResult) {
            if (!placementId.Equals(this.PlacementID)) return;
            OnClose(showResult == ShowResult.Finished);
        }
        #endregion
    }
}
