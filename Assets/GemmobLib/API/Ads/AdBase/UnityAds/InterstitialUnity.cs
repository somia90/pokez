﻿
using UnityEngine.Advertisements;

namespace Gemmob.API.Ads {
    public class InterstitialUnity : AdUnity {
        protected override string PlacementID => "video";

        public InterstitialUnity(string AdUnitId, Type type) : base(AdUnitId, type) {
            Request();
        }

        public override void Initialize() {
            Advertisement.AddListener(this);
        }
    }
}