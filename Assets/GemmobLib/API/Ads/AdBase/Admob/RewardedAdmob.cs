﻿using GoogleMobileAds.Api;
using System;

namespace Gemmob.API.Ads {
    public class RewardedAdmob : Admob {
        private RewardedAd rewardedAd;

        private bool getRewarded = false;

        public RewardedAdmob(string AdUnitId, Type type) : base(AdUnitId, type) {
            Request();
        }

        public override void Initialize() {
            this.rewardedAd = new RewardedAd(AdUnitId);

            rewardedAd.OnAdLoaded += OnAdLoaded;
            rewardedAd.OnAdFailedToLoad += OnAdFailedToLoad;
            rewardedAd.OnAdFailedToShow += OnAdFailedToShow;
            rewardedAd.OnAdOpening += OnAdOpening;
            rewardedAd.OnUserEarnedReward += OnUserEarnedReward;
            rewardedAd.OnAdClosed += OnAdClosed;
        }

        protected override void OnRequest() {
            Initialize();
            rewardedAd.LoadAd(GetAdRequest());
        }

        protected override void OnShow() {
            if (IsLoaded) rewardedAd.Show();
        }

        public override bool IsLoaded {
            get {
                if (rewardedAd != null && rewardedAd.IsLoaded()) return true;
                Request();
                return false;
            }
        }

        #region Handle Callback
        protected override void OnAdOpening(object sender, EventArgs args) {
            getRewarded = false;
            base.OnAdOpening(sender, args);
        }

        private void OnUserEarnedReward(object sender, Reward e) {
            getRewarded = true;
            Logs.LogFormat("[ADS] {0}: getRewarded = {1}", type.ToString(), getRewarded);
        }

        protected override void OnAdClosed(object sender, EventArgs args) {
            base.OnClose(getRewarded);
            getRewarded = false;
        }

        #endregion
    }
}