﻿using GoogleMobileAds.Api;
using System;

namespace Gemmob.API.Ads {
    /**<summary>Current SDK: Google Mobile Ads Unity Plugin v4.0.0 (Google Play services 18.2.0)
     * <para>Link: https://github.com/googleads/googleads-mobile-unity/releases </para>
     * </summary>*/
    public abstract class Admob : Ad {
        public override Sponsor sponsor => Sponsor.Admob;

        public Admob(string AdUnitId, Type type) : base(AdUnitId, type) {
            MobileAds.SetiOSAppPauseOnBackground(true);
        }

        protected AdRequest GetAdRequest() {
            var adRequest = new AdRequest.Builder();
#if PRODUCTION_BUILD
            adRequest.AddTestDevice(AdRequest.TestDeviceSimulator)
                    .AddTestDevice(GemmobAdsConfig.Instance.testDeviceId);
#endif
            return adRequest.Build();
        }
        
        #region Handle
        protected virtual void OnAdLoaded(object sender, EventArgs args) {
            OnAdLoaded();
        }

        protected virtual void OnAdFailedToLoad(object sender, AdErrorEventArgs args) {
            OnAdFailedToLoad(args.Message);
        }

        protected virtual void OnAdFailedToShow(object sender, AdErrorEventArgs args) {
            OnAdFailedToShow(args.Message);
        }

        protected virtual void OnAdOpening(object sender, EventArgs args) {
            OnAdOpening();
        }

        protected virtual void OnAdStarted(object sender, EventArgs args) {
            OnAdStarted();
        }

        protected virtual void OnAdClosed(object sender, EventArgs args) {
            OnAdClosed();
        }

        public virtual void OnAdLeavingApplication(object sender, EventArgs args) {
            OnAdLeavingApplication();
        }
        #endregion Handle
    }
}