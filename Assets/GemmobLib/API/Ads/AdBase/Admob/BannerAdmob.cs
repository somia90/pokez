﻿using System;
using GoogleMobileAds.Api;

namespace Gemmob.API.Ads {
    public class BannerAdmob : Admob {

        private BannerView bannerView;

        public AdPosition adPosition;

        public BannerAdmob(string AdUnitId, Type type, AdPosition adPosition = AdPosition.Bottom) : base(AdUnitId, type) {
            this.adPosition = adPosition;
        }

        public override void Initialize() {
            if (bannerView != null) bannerView.Destroy();

            bannerView = new BannerView(AdUnitId, AdSize.SmartBanner, adPosition);
            bannerView.OnAdLoaded += OnAdLoaded;
            bannerView.OnAdFailedToLoad += OnAdFailedToLoad;
            bannerView.OnAdOpening += OnAdOpening;
            bannerView.OnAdClosed += OnAdClosed;
            bannerView.OnAdLeavingApplication += OnAdLeavingApplication;
        }

        protected override void OnRequest() {
            Initialize();
            bannerView.LoadAd(GetAdRequest());
        }

        protected override void OnShow() {
            if (bannerView == null) {
                Request();
                return;
            }

            bannerView.Show();
        }

        public override bool IsLoaded {
            get {
                if (bannerView != null) return true;
                Request();
                return false;
            }
        }

        public float GetHeight() {
#if UNITY_EDITOR
            float screenHeight = UnityEditor.Handles.GetMainGameViewSize().y;
            var canvas = UnityEngine.GameObject.FindObjectOfType<UnityEngine.UI.CanvasScaler>();
            if (canvas != null) screenHeight = canvas.referenceResolution.y;
            return screenHeight > 720 ? 90 : screenHeight > 400 ? 50 : 32;
#else
            return bannerView != null ? bannerView.GetHeightInPixels() : 0;
#endif
        }

        public override void Hide() {
            if (bannerView != null)
                bannerView.Hide();
        }

        private void OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args) {
            base.OnLoadFailed(args.Message);
        }
    }
}