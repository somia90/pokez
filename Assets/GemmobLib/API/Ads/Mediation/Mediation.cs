﻿using GoogleMobileAds.Api;
using System;
using Gemmob.API.Ads;
using Gemmob.Common.Data;

public partial class Mediation : Singleton<Mediation> {
    #region REMOVE ADS
    private const string NO_ADS = "NO_ADS";
    public static bool NoAds {
        get { return PersitenData.GetBool(NO_ADS); }
        private set { PersitenData.SetBool(NO_ADS, value); }
    }

    public static void BuyRemoveAds() {
        NoAds = true;
    }
    #endregion

    #region Property
    private static AdsConfig.AdmobInfo AdmobInfo => AdsSetting.LoadLocalAdmobInfo();
    private static AdsConfig.BackupInfo BackupInfo => AdsSetting.LoadLocalBackupInfo();

    #endregion

    protected override void Initialize() {
        base.Initialize();
#if ADS_ENABLE
        InitInterstitial();
        InitRewardedVideo();
        InitBannerTop();
        InitBannerBottom();
        MobileAds.Initialize(AdmobInfo.admob_id);
        Logs.Log("[Mediation] Initialized.");
#endif
    }

    public void Show(string position, Ad ads, Ad backup = null, Action onComplete = null, Action onFail = null, float delayTime = 0) {
#if ADS_ENABLE
        if (ads == null) {
            Logs.LogError("[ADS] You must call this first: Mediation.Instance.Preload() \n or Add the Bootstrap.cs & enable flag preloadAds into your first Scene.");
            return;
        }

        if (NoAds && ads.type != Admob.Type.Rewarded) {
            Logs.Log("[ADS] Cancel show: User had removed ads. Call onCompleted callback.");
            Callback.CallSchedule(onComplete, delayTime);
            return;
        }

#if UNITY_EDITOR
        Logs.LogFormat("{0} Show position={1}", ads.LogPrefix, position);
        AdsFake.Show(ads, onComplete, onFail);
        return;
#endif
        if (ads.IsLoaded) {
            Logs.LogFormat("{0} Show position={1}", ads.LogPrefix, position);
            ads.Show(onComplete, onFail);
        }
        else if (backup != null && backup.IsLoaded) {
            Logs.LogFormat("{0} Show Backup position={1}", backup.LogPrefix, position);
            backup.Show(onComplete, onFail);
        }
        else {
            Logs.LogFormat("{0} Currently not available to show. Call onFail callback", ads.LogPrefix);
            Callback.CallSchedule(onFail, delayTime);
        }
#else
        Logs.Log("<color=yellow>[ADS] Ads currently is disable by config ADS_ENABLE. Call onFail default.</color>");
        Callback.CallSchedule(onFail);
#endif
    }

}
