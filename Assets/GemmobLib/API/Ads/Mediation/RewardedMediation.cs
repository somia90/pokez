﻿using Gemmob.API.Ads;
using System;
using System.Diagnostics;

public partial class Mediation {
    public const string RewardedCondition = "ADS_REWARDED";

    RewardedAdmob admobRewarded;
    RewardedUnity unityRewarded;

    [Conditional(RewardedCondition)]
    private void InitRewardedVideo() {
        admobRewarded = new RewardedAdmob(AdmobInfo.video, Ad.Type.Rewarded);
        unityRewarded = new RewardedUnity(BackupInfo.unityads, Ad.Type.Rewarded);
        Logs.Log("[Mediation] RewardVideo initialized.");
    }

    [Conditional(RewardedCondition)]
    public void ShowRewardVideoUnity(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        Show(position, unityRewarded, null, onCompleted, onFailed, delayTime);
    }

    [Conditional(RewardedCondition)]
    public void ShowRewardVideo(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        Show(position, admobRewarded, unityRewarded, onCompleted, onFailed, delayTime);
    }

    [Conditional(RewardedCondition)]
    private void RequestRewardVideo() {
        if (admobRewarded != null) admobRewarded.Request();
    }

    public bool HasRewardVideo {
        get {
#if ADS_ENABLE && ADS_REWARDED
            return (admobRewarded != null && admobRewarded.IsLoaded) || (unityRewarded != null && unityRewarded.IsLoaded);
#else
            Logs.Log("Your project config is not useRewardVideo.");
            return false;
#endif
        }
    }
}
