﻿using Gemmob.API.Ads;
using System;
using System.Diagnostics;

public partial class Mediation {
    public const string InterstitialCondition = "ADS_INTERSTITIAL";

    InterstitialAdmob admobInterstitial;
    InterstitialUnity unityInterstitial;

    [Conditional(InterstitialCondition)]
    private void InitInterstitial() {
        admobInterstitial = new InterstitialAdmob(AdmobInfo.interstitial, Ad.Type.Interstitial);
        unityInterstitial = new InterstitialUnity(BackupInfo.unityads, Ad.Type.Interstitial);
        Logs.Log("[Mediation] Interstial initialized.");
    }

    [Conditional(InterstitialCondition)]
    public void ShowInterstitialUnity(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        Show(position, unityInterstitial, null, onCompleted, onFailed, delayTime);
    }

    [Conditional(InterstitialCondition)]
    public void ShowInterstitial(string position, Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        Show(position, admobInterstitial, unityInterstitial, onCompleted, onFailed ?? onCompleted);
    }

    [Conditional(InterstitialCondition)]
    public void ShowInterstitialWithPercent(int percent, string position = "", Action onCompleted = null, Action onFailed = null, float delayTime = 0) {
        if (admobInterstitial != null && UnityEngine.Random.Range(0, 101) <= percent) {
            ShowInterstitial(position, onCompleted, onFailed, delayTime);
            return;
        }
    }
    
    [Conditional(InterstitialCondition)]
    public void HideInterstitial() {
        if (admobInterstitial != null) admobInterstitial.Hide();
    }

    public bool HasInterstitial {
        get {
#if ADS_ENABLE && ADS_INTERSTITIAL
            return (admobInterstitial != null && admobInterstitial.IsLoaded) || (unityInterstitial != null && unityInterstitial.IsLoaded);
#else
            Logs.Log("Your project config is not useInterstitial.");
            return false;
#endif
        }
    }

}
