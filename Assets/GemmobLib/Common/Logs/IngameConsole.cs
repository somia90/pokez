﻿using System.Collections.Generic;
using UnityEngine;

/** <summary> A console to display Unity's debug logs in-game. 
 * <para>Hold to any point on the screeen in 5s to Show the Ingame Console</para>
 * </summary> */
public class IngameConsole : SingletonFreeAlive<IngameConsole> {
    private List<Log> logs = new List<Log>();

    private bool show;
    private bool collapse;

    private const int margin = 20;

    private Vector2 scrollPosition;
    Rect windowRect = new Rect(margin, margin, Screen.width - (margin * 2), Screen.height/3 - margin);
    
    void OnEnable() {
        Application.logMessageReceived += RecordLog;
    }

    void OnDisable() {
        Application.logMessageReceived -= RecordLog;
    }

    public void Show() {
        show = true;
        //gameObject.SetActive(true);
    }

    public void Hide() {
        show = false;
        //gameObject.SetActive(false);
    }

    private void RecordLog(string message, string stackTrace, LogType type) {
        logs.Add(new Log(message, stackTrace, type));
    }

    private void OnGUI() {
        if (!show) {
            return;
        }

        windowRect = GUILayout.Window(123456, windowRect, ShowConsoleWindow, "Console");
    }

    private void ShowConsoleWindow(int windowID) {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        {
            for (int i = 0; i < logs.Count; i++) {
                var log = logs[i];
                if (collapse && i > 0 && log.message.Equals(logs[i - 1].message)) {
                    continue;
                }

                GUI.contentColor = logTypeColors[log.type];
                GUILayout.Label(string.Format("{0}: {1}", log.time, log.message));
            }
        }
        GUILayout.EndScrollView();

        GUI.contentColor = Color.white;

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Clear")) {
            logs.Clear();
        }

        if (GUILayout.Button("Hide")) {
            Hide();
        }

        collapse = GUILayout.Toggle(collapse, "Collapse", GUILayout.ExpandWidth(false));

        GUILayout.EndHorizontal();

        GUI.DragWindow(windowRect);
    }

    #region define
    struct Log {
        public string time;
        public string message;
        public string stackTrace;
        public LogType type;

        public Log(string message, string stackTrace, LogType type) {
            this.time = System.DateTime.Now.ToShortTimeString();
            this.message = message;
            this.stackTrace = stackTrace;
            this.type = type;
        }
    }

    static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color>() {
        { LogType.Assert, Color.red },
        { LogType.Error, Color.red },
        { LogType.Exception, Color.red },
        { LogType.Log, Color.white },
        { LogType.Warning, Color.yellow },
    };
    #endregion


    #region Check hold to show
    bool holdToShow = false;
    private void Update() {
        if (show) return;
        if (Input.GetMouseButtonDown(0)) {
            CheckBeginHold();
        }
        else if (Input.GetMouseButtonUp(0)) {
            CheckEndHold();
        }
    }

    private void CheckBeginHold() {
        if (holdToShow || (UnityEngine.EventSystems.EventSystem.current != null && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())) return;

        holdToShow = true;
        //Logs.Log("[IngameConsole] Keep holding for 5s to show the Logcat ingame!");

        if (IsInvoking("CheckHoldingTimeToShow")) CancelInvoke("CheckHoldingTimeToShow");
        Invoke("CheckHoldingTimeToShow", 5);
    }

    public void CheckEndHold() {
        if (!holdToShow) return;
        holdToShow = false;
    }

    private void CheckHoldingTimeToShow() {
        if (!holdToShow) return;
        Show();
        holdToShow = false;
    }
    #endregion
}