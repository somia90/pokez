﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using TMPro;
using System;
using System.Text.RegularExpressions;

public class UIController : MonoBehaviour
{
    public enum Popup
    {
        GameOver,
        Restart,
        Reward,
        Deck,
        Option,
        Undo
    }
    
    [Header("Popups")]
    [SerializeField] public VPanel[] popups = null;

    [Header("Game Objects")]
    [SerializeField] private GameObject _blackBG = null;
    [SerializeField] private GameObject _undoBtn = null;
    [SerializeField] private GameManager _gameManager = null;

    [Header("Texts")]
    [SerializeField] public TextMeshProUGUI currentScoreText = null;
    [SerializeField] public TextMeshProUGUI bestScoreText = null;
    [SerializeField] private TextMeshPro _effectText = null;

    private List<TextMeshPro> _effectTextPool;

    private int _currentPopup;

    private void Start()
    {
        _effectTextPool = new List<TextMeshPro>();

        _blackBG.GetComponent<Image>().color = new Color32(0, 0, 0, 0);
        _blackBG.gameObject.SetActive(false);

        foreach(VPanel p in popups)
        {
            p.gameObject.SetActive(false);
        }
    }

    public void OpenPopup(int _id)
    {
        if (_currentPopup != 2) VSoundManager.Instance.Play(GameManager.sfx.UISelectFX);

        _gameManager.AllowInput(false);

        _blackBG.gameObject.SetActive(true);
        _blackBG.GetComponent<Image>().DOFade(0.9f, 0.25f);

        popups[_currentPopup].GetComponent<VPanel>().ClosePopup();
        popups[_id].GetComponent<VPanel>().OpenPopup();

        _currentPopup = _id;

        if (_id == 7)
        {
            popups[_id].GetComponent<HandListPopup>().SetSprite();
        }
    }

    public void ClosePopup()
    {
        _gameManager.AllowInput(true);

        _blackBG.GetComponent<Image>().DOFade(0f, 0.25f).OnComplete(() =>
        {
            _blackBG.gameObject.SetActive(false);
        });

        if (_currentPopup == 2 && _gameManager.IsLose) 
            _gameManager.GameOver();
        else 
            VSoundManager.Instance.Play(GameManager.sfx.UISelectFX);

        popups[_currentPopup].GetComponent<VPanel>().ClosePopup();
    }

    public void AllowUndo(bool val)
    {
        _undoBtn.SetActive(val);
    }

    public void AddTextEffect(Vector3 _pos, String _text, String _point)
    {
        var effText = _effectTextPool.Count > 0 ? _effectTextPool.PopAt(0) : Instantiate(_effectText);
        effText.name = "TextAnim";
        effText.gameObject.SetActive(true);
        effText.transform.position = _pos;
        effText.text = "<color=#F2D230>" + this.SplitCamelCase(_text) + "\n<color=#FFFFFF>+" + _point;
        effText.GetComponent<MeshRenderer>().sortingLayerName = "Text";

        effText.DOFade(1, 0.3f).OnComplete(() =>
        {
            effText.DOFade(0, 0.3f).SetDelay(1.2f).OnComplete(() =>
            {
                effText.gameObject.SetActive(false);
                _effectTextPool.Add(effText);
            });
        });
        effText.transform.DOMoveY(effText.transform.position.y + 1.5f, 1.8f).SetEase(Ease.OutExpo);
    }

    public string SplitCamelCase(string str)
    {
        return Regex.Replace(Regex.Replace(str, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1 $2"), @"(\p{Ll})(\P{Ll})", "$1 $2");
    }

    public void SetPanelColor(Color c)
    {
        for (int i = 0; i < popups.Length; i++)
        {
            popups[i].GetComponent<Image>().color = c;
        }
    }
}
