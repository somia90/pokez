﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static class IListExtension
{
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

    public static T PopAt<T>(this List<T> list, int index)
    {
        T r = list[index];
        list.RemoveAt(index);
        return r;
    }

    public static void AddToFront<T>(this List<T> list, T item)
    {
        // omits validation, etc.
        list.Insert(0, item);
    }   
}
