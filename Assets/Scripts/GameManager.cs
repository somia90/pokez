﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Linq;
using DG.Tweening;
using GoogleMobileAds.Api;

public class GameManager : MonoBehaviour
{
    [Header("Game Objects")]
    [SerializeField] private Deck _deck = null;
    [SerializeField] private ParticleSystem _confetti = null;

    [Header("UI Controller")]
    [SerializeField] private UIController _uiController = null; 
    
    [Header("Hands")]
    [SerializeField] private CardGroup[] _cardgroup = null;

    [Header("Table Slots")]
    [SerializeField] private GameObject _table = null;

    [Header("Tutorial UI")]
    [SerializeField] private TutorialUI _tutorialUI = null;


    private Card[] _card;
    private Slot[] _slot = null;

    private Combo _combo;

    private int _cardGroupUseCounter;
    private int _bestScore;
    private int _currentScore;
    private int _nextScore;
    private int _comboCount;
    private int _currentPack;
    private int _scoreSFX;
    private int _tutorialStep;
    
    private float _scoreTick = 0.05f;
    
    private bool _isLose;
    private bool _watchAd;
    private bool _tutorial;
    
    private string _currentState;
    private string _previousState;

    private SaveState _state;
    
    public Slot[] slot { get { return _slot; } }
    public int currentPack { get { return _currentPack; } set { _currentPack = value; } }
    public bool IsLose { get { return _isLose; } }
    public int level { get; set; }
    public int selectPack { get; set; }

    public bool tutorial { get { return _tutorial; } }

    public int cardGroupUseCounter { get { return _cardGroupUseCounter; } }

    public static SFX sfx;

    private void Awake()
    {
        sfx = new SFX();

        _isLose = true;
        _watchAd = false;
        level = 0;
        _comboCount = 0;
        _nextScore = 0;
        _cardGroupUseCounter = 0;

        _state = new SaveState();

        _bestScore = PlayerPrefs.HasKey("BestScore") ? PlayerPrefs.GetInt("BestScore") : 300;
        selectPack = _currentPack = PlayerPrefs.HasKey("CurrentPack") ? PlayerPrefs.GetInt("CurrentPack") : 0;

        _currentState = PlayerPrefs.HasKey("CurrentState") ? PlayerPrefs.GetString("CurrentState") : "";
        _previousState = "";

        _tutorial = !PlayerPrefs.HasKey("Tutorial");

        Mediation.Instance.Preload();
    }

    private void Start()
    {
        _combo = new Combo();

        _deck.UpdateCardPack((Deck.Pack)_currentPack);
        _uiController.bestScoreText.text = (_bestScore * 10).ToString();

        _uiController.AllowUndo(false);

        var _slotCount = _table.transform.childCount;

        _slot = new Slot[_slotCount];

        for (int i = 0; i < _slotCount; i++)
        {
            _slot[i] = _table.transform.GetChild(i).GetComponent<Slot>();
        }

        _scoreSFX = 0;

        if (_tutorial)
        {
            TextAsset _tutJson = Resources.Load<TextAsset>("level/tutorial");
            this.LoadData(_tutJson.text);

            _tutorialStep = 0;
            this.StartTutorial();
        }
        else
        {
            Destroy(_tutorialUI.gameObject);

            if (_currentState != "")
                this.LoadData(_currentState);

            if (_isLose)
                this.NewGame();
            else
                this.UpdateCurrentCard();
        }

        Mediation.Instance.ShowBannerTop("");
    }

    private void Update()
    {
        _scoreTick -= Time.deltaTime;

        if (_scoreTick <= 0f)
        {
            this.RefreshUIText();
            _scoreTick = 0.05f;
        }

        if (!_confetti.IsAlive())
        {
            _confetti.gameObject.SetActive(false);
        }
    }

    public void NewGame()
    {
        if (level >= 1 && Mediation.Instance.HasInterstitial)
        {
            Mediation.Instance.ShowInterstitial(AdPosition.Center.ToString(), this.Init, this.Init);
        }
        else
        {
            this.Init();
        }
    }

    private void Init()
    {
        _isLose = false;
        _watchAd = false;
        level = 0;
        _comboCount = 0;
        _cardGroupUseCounter = 0;
        _currentScore = _nextScore = 0;

        foreach (Slot s in _slot)
        {
            s.Refresh();
        }

        _uiController.currentScoreText.text = "0";

        _deck.PrepareDeck();
        this.SpawnCardGroup();

        _deck.GetComponent<SpriteRenderer>().sharedMaterial.SetFloat("_GrayScale_Fade_1", 0f);

        this.SaveData();
    }

    private void RefreshUIText()
    {
        if (_currentScore < _nextScore)
        {
            _currentScore++;
            _uiController.currentScoreText.text = (_currentScore * 10).ToString();
        }
        else if (_currentScore >= _nextScore && _currentScore != 0)
        {
            _currentScore = _nextScore;          
        }

        if (_nextScore > _bestScore)
        {
            _bestScore = _nextScore;
            _uiController.bestScoreText.text = (_bestScore * 10).ToString();
            PlayerPrefs.SetInt("BestScore", _bestScore);
        }
    }

    public void addPoint(Combo.ComboType _type, int _repeat = 0)
    {
        _nextScore += (int)Math.Pow(2, (double)_type) * (_repeat + 1);
    }

    // Use this function to display card sprite on cards based on card data
    private void GetCard(Card card, int[] _cardData, bool flip = true)
    {
        card.rank = (Card.Rank)_cardData[0];
        card.suit = (Card.Suit)_cardData[1];

        card.Showcard(_deck.CardBackSprite(), _deck.CardSprite(card.rank, card.suit), flip);
    }

    // Use this function to update the whole table to a new card pack
    public void UpdateCurrentCard()
    {
        if (selectPack != _currentPack)
        {
            if (Mediation.Instance.HasRewardVideo)
            {
                Mediation.Instance.ShowRewardVideo(AdPosition.Center.ToString(), this.ExecuteChangePack, ()=> { _uiController.OpenPopup(6); });
            }
            else
            {
                _uiController.OpenPopup(6);
            }
            
        }
    }

    private void ExecuteChangePack()
    {
        _currentPack = selectPack;
        PlayerPrefs.SetInt("CurrentPack", _currentPack);

        _deck.UpdateCardPack((Deck.Pack)_currentPack);

        GameObject[] _cardObject = GameObject.FindGameObjectsWithTag("Card");

        for (int i = 0; i < _cardObject.Length; i++)
        {
            ICard _c = _cardObject[i].GetComponent<ICard>();
            _c.SetSprite(_deck.CardSprite(_c.rank, _c.suit));
        }
    }

    public void CheckSpawnCard()
    {
        _cardGroupUseCounter++;
        if (_cardGroupUseCounter >= 3)
        {
            _cardGroupUseCounter = 0;
            this.SpawnCardGroup();
        }
    }

    // Create to craete 3 new card groups everytime player has placed all remaining card group on table.
    private void SpawnCardGroup()
    {
        for (int i = 0; i < _cardgroup.Length; i++)
        {
            _cardgroup[i].gameObject.SetActive(true);
            var percent = Random.Range(0, 3 + level);

            // We need to create randomly between 3 types of groups, get card data from deck and display them on cards.
            if (percent < 2)
            {
                _cardgroup[i].PrepareCardGroup(CardGroup.GroupType.Single);

                this.GetCard(_cardgroup[i].GetCard1(), _deck.GetCardData());
            }
            else
            {
                if (Random.Range(0, 2) < 1)
                    _cardgroup[i].PrepareCardGroup(CardGroup.GroupType.Hor2);
                else
                    _cardgroup[i].PrepareCardGroup(CardGroup.GroupType.Ver2);

                this.GetCard(_cardgroup[i].GetCard1(), _deck.GetCardData());
                this.GetCard(_cardgroup[i].GetCard2(), _deck.GetCardData());
            }

            this.TweenCardGroup(_cardgroup[i], i * 0.1f);
        }
    }

    private void TweenCardGroup(CardGroup c, float delay = 0f)
    {
        c.transform.position -= 5 * transform.up;
        c.transform.DOMove(c.InitPos, 0.4f).SetEase(Ease.OutQuad).SetDelay(delay).OnComplete(() =>
        {
            VSoundManager.Instance.Play(sfx.CardFlipFX);
        });
    }

    // Check if there is enough space in the table to put on atleast 1 remaining card group
    public void CheckLoseCondition()
    {
        _isLose = true;

        for (int i = 0; i < _cardgroup.Length; i++)
        {
            if (_cardgroup[i].CanDrag)
            {
                for (int j = 0; j < _slot.Length; j++)
                {
                    switch (_cardgroup[i].Type)
                    {
                        case CardGroup.GroupType.Single:
                            if (_slot[j].IsEmpty)
                                _isLose = false;
                            break;
                        case CardGroup.GroupType.Hor2:
                            if (_slot[j].Pos.x < 4 && _slot[j].IsEmpty && _slot[j + 1].IsEmpty)
                                _isLose = false;
                            break;
                        case CardGroup.GroupType.Ver2:
                            if (_slot[j].Pos.y < 4 && _slot[j].IsEmpty && _slot[j + 5].IsEmpty)
                                _isLose = false;
                            break;
                    }
                }
            }

            if (!_isLose)
                break;
        }

        if (_isLose)
        {
            if (!_watchAd && Mediation.Instance.HasRewardVideo)
            {
                Invoke("OfferRevive", 0.6f);
            }
            else
                this.GameOver();
        }
    }

    public void OfferRevive()
    {
        _uiController.OpenPopup((int)UIController.Popup.Reward);
    }

    public void RemoveFiveCards()
    {
        _watchAd = true;
        _isLose = false;

        Mediation.Instance.ShowRewardVideo(AdPosition.Center.ToString(), this.ExecuteRemoveFiveCard, this.GameOver);
    }

    private void ExecuteRemoveFiveCard()
    {
        var _remainCard = 5;
        for (int i = 0; i < _slot.Length; i++)
        {
            if (_slot[i].IsEmpty == false)
            {
                if (Random.Range(0, 2) == 0)
                {
                    _remainCard--;
                    _combo.cardRemove.Add(_slot[i]);
                }
            }

            if (_remainCard == 0)
                break;
        }

        this.CheckRemoveCard();
        this.CheckLoseCondition();

        this.SaveData();
    }

    public void GameOver()
    {
        Debug.Log("You Lose");
        VSoundManager.Instance.Play(sfx.GameOverFX);

        var material = _deck.GetComponent<SpriteRenderer>().sharedMaterial;
        DOTween.To(() => material.GetFloat("_GrayScale_Fade_1"), value =>
        {
            material.SetFloat("_GrayScale_Fade_1", value);
        }, 1f, 2f).OnComplete(() => 
        {
            _uiController.OpenPopup((int)UIController.Popup.GameOver);

            String str = "<color=#F2D230>Score" + "\n<color=#FFFFFF>" + _nextScore * 10;
            _uiController.popups[0].GetComponent<GameOverPopup>().SetText(str, _nextScore == _bestScore);
        });
    }

    // Only remove cards in combinations after we check all rows/column associate with placed cards.
    public void CheckRemoveCard()
    {
        if (_combo.cardRemove.Count() == 0)
        {
            _scoreSFX = 0;
            return;
        }
        else
        {
            VSoundManager.Instance.Play(sfx.ScoreFX[_scoreSFX]);
            _scoreSFX++;
            if (_scoreSFX >= sfx.ScoreFX.Length) _scoreSFX = sfx.ScoreFX.Length - 1;
        }
        
        foreach (Slot c in _combo.cardRemove)
        {
            if (!c.IsEmpty)
            {
                _deck.ComboAnim(c.transform.position.x, c.transform.position.y, (int)c.rank, (int)c.suit);
                
                c.IsEmpty = true;
                _deck.ReturnCard(c.DisposeCard());
            }
        }

        _combo.ClearCard();
    }

    // Check if the row/column of cards form any combination
    public void CheckCombo(List<Slot> combo)
    {
        _combo.CheckCombo(combo);

        if (_combo.type != Combo.ComboType.HighCard)
        {
            _comboCount++;
            level = _comboCount / 10;

            this.addPoint(_combo.type);
            Debug.Log(_combo.comboString);
            
            Vector3 _pos = new Vector3();

            foreach (Slot c in combo)
            {
                _pos += c.transform.position;
            }

            _pos.x /= 5f;
            _pos.y /= 5f;
            
            _uiController.AddTextEffect(_pos, _combo.type.ToString() , (Math.Pow(2, (double)_combo.type) * 10).ToString());
        }

        if ((int)_combo.type >= (int)Combo.ComboType.Flush)
        {
            _confetti.gameObject.SetActive(true);
            _confetti.Play();
            VSoundManager.Instance.Play(sfx.ConfettiFX);
        }
    }

    public void AllowInput(bool canPlay)
    {
        foreach(CardGroup g in _cardgroup)
        {
            g.allowInput = canPlay;
        }
    }

    public Color GetThemeColor()
    {
        return _deck.GetThemeColor((Deck.Pack)_currentPack);
    }

    public void SetThemeColor(Deck.Pack pack)
    {
        _deck.SetThemeColor(pack);
    }

    public void SaveData()
    {
        if (_currentState != "")
        {
            _previousState = _currentState;
            if (Mediation.Instance.HasRewardVideo)
            {
                _uiController.AllowUndo(true);
            }
        }

        _state.lose = _isLose;
        _state.ad = _watchAd;
        _state.combo = _comboCount;
        _state.score = _nextScore;
        _state.deal = _cardGroupUseCounter;
        _state.lv = level;
        _state.turn = _deck.getCardCounter;
        _state.deck = _deck.CurrentDeck();

        for (int i = 0; i < _slot.Length; i++)
        {
            _state.slot[i].r = (int)_slot[i].rank;
            _state.slot[i].s = (int)_slot[i].suit;
            _state.slotState[i] = _slot[i].IsEmpty ? 0 : 1;
        }

        for (int i = 0; i < _cardgroup.Length; i++)
        {
            _state.group[i].c1.r = (int)_cardgroup[i].GetCard1().rank;
            _state.group[i].c1.s = (int)_cardgroup[i].GetCard1().suit;

            _state.group[i].c2.r = (int)_cardgroup[i].GetCard2().rank;
            _state.group[i].c2.s = (int)_cardgroup[i].GetCard2().suit;

            _state.group[i].drag = _cardgroup[i].CanDrag;
            _state.group[i].type = (int)_cardgroup[i].Type;
        }

        _currentState = JsonUtility.ToJson(_state);
        PlayerPrefs.SetString("CurrentState", _currentState);
    }

    public void LoadData(string str)
    {
        _state = JsonUtility.FromJson<SaveState>(str);

        _isLose = _state.lose;
        _watchAd = _state.ad;
        _comboCount = _state.combo;
        _currentScore = _nextScore = _state.score;
        _cardGroupUseCounter = _state.deal;

        level = _state.lv;

        _uiController.currentScoreText.text = (_currentScore * 10).ToString();

        _deck.LoadDeck(_state.deck);
        _deck.getCardCounter = _state.turn;

        for (int i = 0; i < _slot.Length; i++)
        {
            if (_state.slotState[i] == 0)
                _slot[i].Refresh();
            else
            {
                int[] c = { _state.slot[i].r, _state.slot[i].s };
                _slot[i].SetCard(c);
            }   
        }

        for (int i = 0; i < _cardgroup.Length; i++)
        {
            _cardgroup[i].Type = (CardGroup.GroupType)_state.group[i].type;
            _cardgroup[i].Refresh(_cardgroup[i].Type);

            int[] card1 = { _state.group[i].c1.r, _state.group[i].c1.s };
            int[] card2 = { _state.group[i].c2.r, _state.group[i].c2.s };

            this.GetCard(_cardgroup[i].GetCard1(), card1, false);
            this.GetCard(_cardgroup[i].GetCard2(), card2, false);

            _cardgroup[i].gameObject.SetActive(_state.group[i].drag);
            _cardgroup[i].CanDrag = _state.group[i].drag;
        }

        _deck.GetComponent<SpriteRenderer>().sharedMaterial.SetFloat("_GrayScale_Fade_1", 0f);
    }

    public void Undo()
    {
        Mediation.Instance.ShowRewardVideo(AdPosition.Center.ToString(), this.ExecuteUndo);
    }

    private void ExecuteUndo()
    {
        if (_previousState != "")
        {
            _uiController.AllowUndo(false);
            this.LoadData(_previousState);
            this.UpdateCurrentCard();
        }

        _currentState = _previousState;
        _previousState = "";
    }

    public void StartTutorial()
    {
        _uiController.gameObject.SetActive(false);
        
        _tutorialUI.gameObject.SetActive(true);

        foreach(CardGroup c in _cardgroup)
        {
            c.gameObject.SetActive(false);
        }

        this.PrepareNextTut(_tutorialStep);
    }

    public void PrepareNextTut(int step)
    {
        _cardgroup[step].gameObject.SetActive(true);
        this.TweenCardGroup(_cardgroup[step]);

        _tutorialUI.SetUI(step, _cardgroup[step].InitPos);
        _tutorialUI.ShowText(step);
    }

    public void CheckEndTut()
    {
        _tutorialStep++;
        if (_tutorialStep < 3)
            this.PrepareNextTut(_tutorialStep);
        else
        {
            _tutorialUI.ShowText(_tutorialStep);
            _isLose = true;
            Invoke("FinishTut", 2.5f);
        }
    }

    public void FinishTut()
    {
        _tutorialUI.gameObject.SetActive(false);
        _uiController.gameObject.SetActive(true);

        _tutorial = false;
        PlayerPrefs.SetInt("Tutorial", _tutorialStep);

        _bestScore = 0;
        _nextScore = 300;
        this.RefreshUIText();

        //_uiController.OpenPopup(6);
        this.NewGame();

        Destroy(_tutorialUI.gameObject);
        _uiController.OpenPopup(8);
    }
}
