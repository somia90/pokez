﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX
{
    public AudioClip UISelectFX;
    public AudioClip CardFlipFX;
    public AudioClip ErrorMoveFX;
    public AudioClip GameOverFX;
    public AudioClip ConfettiFX;
    public AudioClip[] CardDealFX;
    public AudioClip[] ScoreFX;
    
    public SFX()
    {
        UISelectFX = Resources.Load<AudioClip>("Sounds/UISelect");
        CardFlipFX = Resources.Load<AudioClip>("Sounds/cardFlip");
        ErrorMoveFX = Resources.Load<AudioClip>("Sounds/error");
        GameOverFX = Resources.Load<AudioClip>("Sounds/gameOver");
        ConfettiFX = Resources.Load<AudioClip>("Sounds/ConfettiBomb");

        CardDealFX = new AudioClip[3];
        for (int i = 0; i < CardDealFX.Length; i++)
        {
            CardDealFX[i] = Resources.Load<AudioClip>("Sounds/cardDeal" + (i + 1));
        }

        ScoreFX = new AudioClip[10];
        for (int i = 0; i < ScoreFX.Length; i++)
        {
            ScoreFX[i] = i < 9 ? Resources.Load<AudioClip>("Sounds/score_0" + (i + 1)) : Resources.Load<AudioClip>("Sounds/score_0" + i);
        }
    }
}
