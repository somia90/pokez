﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Combo
{
    public enum ComboType
    {
        HighCard = 0,
        Pair = 1,
        TwoPair = 2,
        ThreeOfAKind = 3,
        Straight = 4,
        Flush = 5,
        FullHouse = 6,
        FourOfAKind = 7,
        StraightFlush = 8,
        RoyalFlush = 9
    };

    public List<Slot> cardRemove;
    public string comboString;
    public ComboType type;

    public Combo()
    {
        cardRemove = new List<Slot>();
    }

    public void ClearCard()
    {
        cardRemove.Clear();
    }

    public void CheckCombo(List<Slot> combo)
    {
        bool _fullLineCombo = true;

        type = ComboType.HighCard;

        comboString = "";

        // Check if the row/column of cards contains any empty Slot
        // If there is atleast 1 empty Slot, we can check a few combinations only
        foreach (Slot c in combo)
        {
            if (c.IsEmpty)
            {
                _fullLineCombo = false;
                break;
            }
        }

        if (_fullLineCombo)
        {
            if (CheckRoyalFlush(combo)) type = ComboType.RoyalFlush;
            else if (CheckStraightFlush(combo)) type = ComboType.StraightFlush;
            else if (CheckFourOfAKind(combo)) type = ComboType.FourOfAKind;
            else if (CheckFullHouse(combo)) type = ComboType.FullHouse;
            else if (CheckFlush(combo)) type = ComboType.Flush;
            else if (CheckLowStraight(combo) || CheckHighStraight(combo)) type = ComboType.Straight;
            else if (CheckThreeOfAKind(combo)) type = ComboType.ThreeOfAKind;
            else if (CheckTwoPair(combo)) type = ComboType.TwoPair;
            else if (CheckPair(combo)) type = ComboType.Pair;
        }
        else
        {
            if (CheckCloseQuad(combo)) type = ComboType.FourOfAKind;
            else if (CheckCloseTrip(combo)) type = ComboType.ThreeOfAKind;
            else if (CheckCloseTwoPair(combo)) type = ComboType.TwoPair;
            else if (CheckClosePair(combo)) type = ComboType.Pair;
        }

        comboString += "[" + type.ToString() + "]";
    }

    public bool CheckClosePair(List<Slot> cards)
    {
        for (int i = 0; i < cards.Count - 1; i++)
        {
            if (!cards[i].IsEmpty && !cards[i + 1].IsEmpty && cards[i].rank == cards[i + 1].rank)
            {
                cardRemove.Add(cards[i]);
                cardRemove.Add(cards[i + 1]);

                comboString += cards[i].rank + " " + cards[i].suit + ", ";
                comboString += cards[i + 1].rank + " " + cards[i + 1].suit + ", ";

                return true;
            }
        }

        comboString = "";
        return false;
    }

    public bool CheckCloseTwoPair(List<Slot> cards)
    {
        if (cards[0].IsEmpty)
        {
            cards.RemoveAt(0);
        }
        else if (cards[4].IsEmpty)
        {
            cards.RemoveAt(4);
        }
        else
        {
            return false;
        }

        for (int i = cards.Count(); i > 0; i--)
        {
            if (cards[i - 1].IsEmpty)
            {
                return false;
            }
        }

        if (cards[0].rank == cards[1].rank && cards[2].rank == cards[3].rank)
        {
            cardRemove.Add(cards[0]);
            cardRemove.Add(cards[1]);
            cardRemove.Add(cards[2]);
            cardRemove.Add(cards[3]);

            comboString += cards[0].rank + " " + cards[0].suit + ", ";
            comboString += cards[1].rank + " " + cards[1].suit + ", ";
            comboString += cards[2].rank + " " + cards[2].suit + ", ";
            comboString += cards[3].rank + " " + cards[3].suit + ", ";

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckCloseTrip(List<Slot> cards)
    {
        for (int i = 0; i < cards.Count - 2; i++)
        {
            if (!cards[i].IsEmpty && !cards[i + 1].IsEmpty && !cards[i + 2].IsEmpty && cards[i].rank == cards[i + 1].rank && cards[i].rank == cards[i + 2].rank)
            {
                cardRemove.Add(cards[i]);
                cardRemove.Add(cards[i + 1]);
                cardRemove.Add(cards[i + 2]);

                comboString += cards[i].rank + " " + cards[i].suit + ", ";
                comboString += cards[i + 1].rank + " " + cards[i + 1].suit + ", ";
                comboString += cards[i + 2].rank + " " + cards[i + 2].suit + ", ";

                return true;
            }
        }

        comboString = "";
        return false;
    }

    public bool CheckCloseQuad(List<Slot> cards)
    {
        for (int i = 0; i < cards.Count - 3; i++)
        {
            if (!cards[i].IsEmpty && !cards[i + 1].IsEmpty && !cards[i + 2].IsEmpty && !cards[i + 3].IsEmpty && cards[i].rank == cards[i + 1].rank && cards[i].rank == cards[i + 2].rank && cards[i].rank == cards[i + 3].rank)
            {
                cardRemove.Add(cards[i]);
                cardRemove.Add(cards[i + 1]);
                cardRemove.Add(cards[i + 2]);
                cardRemove.Add(cards[i + 3]);

                comboString += cards[i].rank + " " + cards[i].suit + ", ";
                comboString += cards[i + 1].rank + " " + cards[i + 1].suit + ", ";
                comboString += cards[i + 2].rank + " " + cards[i + 2].suit + ", ";
                comboString += cards[i + 3].rank + " " + cards[i + 3].suit + ", ";

                return true;
            }
        }

        comboString = "";
        return false;
    }

    public bool CheckPair(List<Slot> cards)
    {
        if (cards.GroupBy(slot => slot.rank).Count(group => group.Count() == 2) == 1)
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            for (int i = 0; i < _cards.Count() - 1; i++)
            {
                if (_cards[i].rank == _cards[i + 1].rank)
                {
                    cardRemove.Add(_cards[i]);
                    cardRemove.Add(_cards[i + 1]);

                    comboString += _cards[i].rank + " " + _cards[i].suit + ", ";
                    comboString += _cards[i + 1].rank + " " + _cards[i + 1].suit + ", ";
                    break;
                }
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckTwoPair(List<Slot> cards)
    {
        if (cards.GroupBy(slot => slot.rank).Count(group => group.Count() == 2) == 2)
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            if (_cards[0].rank == _cards[1].rank)
            {
                cardRemove.Add(_cards[0]);
                cardRemove.Add(_cards[1]);

                comboString += _cards[0].rank + " " + _cards[0].suit + ", ";
                comboString += _cards[1].rank + " " + _cards[1].suit + ", ";

                if (_cards[2].rank == _cards[3].rank)
                {
                    cardRemove.Add(_cards[2]);
                    cardRemove.Add(_cards[3]);

                    comboString += _cards[2].rank + " " + _cards[2].suit + ", ";
                    comboString += _cards[3].rank + " " + _cards[3].suit + ", ";
                }
                else
                {
                    cardRemove.Add(_cards[3]);
                    cardRemove.Add(_cards[4]);

                    comboString += _cards[3].rank + " " + _cards[3].suit + ", ";
                    comboString += _cards[4].rank + " " + _cards[4].suit + ", ";
                }
            }
            else
            {
                cardRemove.Add(_cards[1]);
                cardRemove.Add(_cards[2]);
                cardRemove.Add(_cards[3]);
                cardRemove.Add(_cards[4]);

                comboString += _cards[1].rank + " " + _cards[1].suit + ", ";
                comboString += _cards[2].rank + " " + _cards[2].suit + ", ";
                comboString += _cards[3].rank + " " + _cards[3].suit + ", ";
                comboString += _cards[4].rank + " " + _cards[4].suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckThreeOfAKind(List<Slot> cards)
    {
        if (cards.GroupBy(slot => slot.rank).Any(group => group.Count() == 3))
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            for (int i = 0; i < _cards.Count() - 2; i++)
            {
                if (_cards[i].rank == _cards[i + 1].rank && _cards[i].rank == _cards[i + 2].rank)
                {
                    cardRemove.Add(_cards[i]);
                    cardRemove.Add(_cards[i + 1]);
                    cardRemove.Add(_cards[i + 2]);

                    comboString += _cards[i].rank + " " + _cards[i].suit + ", ";
                    comboString += _cards[i + 1].rank + " " + _cards[i + 1].suit + ", ";
                    comboString += _cards[i + 2].rank + " " + _cards[i + 2].suit + ", ";
                    break;
                }
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckHighStraight(List<Slot> cards)
    {
        var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

        if (_cards[0].rank == Card.Rank.King && _cards[1].rank == Card.Rank.Queen && _cards[2].rank == Card.Rank.Jack && _cards[3].rank == Card.Rank.Ten && _cards[4].rank == Card.Rank.Ace)
        {
            foreach (Slot c in _cards)
            {
                cardRemove.Add(c);

                comboString += c.rank + " " + c.suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckLowStraight(List<Slot> cards)
    {
        var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

        if (Enumerable.Range(0, _cards.Count() - 1).All(i => (int)_cards[i].rank - 1 == (int)_cards[i + 1].rank))
        {
            foreach (Slot c in _cards)
            {
                cardRemove.Add(c);

                comboString += c.rank + " " + c.suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckFlush(List<Slot> cards)
    {
        if (cards.GroupBy(slot => slot.suit).Count(group => group.Count() >= 5) == 1)
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            foreach (Slot c in _cards)
            {
                cardRemove.Add(c);

                comboString += c.rank + " " + c.suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckFullHouse(List<Slot> cards)
    {
        if (CheckPair(cards) && CheckThreeOfAKind(cards))
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            comboString = "";

            foreach (Slot c in _cards)
            {
                cardRemove.Add(c);

                comboString += c.rank + " " + c.suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckFourOfAKind(List<Slot> cards)
    {
        if (cards.GroupBy(slot => slot.rank).Any(group => group.Count() == 4))
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            for (int i = 0; i < _cards.Count() - 3; i++)
            {
                if (_cards[i].rank == _cards[i + 1].rank && _cards[i].rank == _cards[i + 2].rank && _cards[i].rank == _cards[i + 3].rank)
                {
                    cardRemove.Add(_cards[i]);
                    cardRemove.Add(_cards[i + 1]);
                    cardRemove.Add(_cards[i + 2]);
                    cardRemove.Add(_cards[i + 3]);

                    comboString += _cards[i].rank + " " + _cards[i].suit + ", ";
                    comboString += _cards[i + 1].rank + " " + _cards[i + 1].suit + ", ";
                    comboString += _cards[i + 2].rank + " " + _cards[i + 2].suit + ", ";
                    comboString += _cards[i + 3].rank + " " + _cards[i + 3].suit + ", ";
                    break;
                }
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckStraightFlush(List<Slot> cards)
    {
        if (CheckLowStraight(cards) && CheckFlush(cards))
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            comboString = "";

            foreach (Slot c in _cards)
            {
                cardRemove.Add(c);

                comboString += c.rank + " " + c.suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }

    public bool CheckRoyalFlush(List<Slot> cards)
    {
        if (CheckHighStraight(cards) && CheckFlush(cards))
        {
            var _cards = cards.OrderByDescending(a => (int)a.rank).ToList();

            comboString = "";

            foreach (Slot c in _cards)
            {
                cardRemove.Add(c);

                comboString += c.rank + " " + c.suit + ", ";
            }

            return true;
        }

        comboString = "";
        return false;
    }
}