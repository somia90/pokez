﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class DeckPopup : VPanel
{
    [SerializeField] private Image _selectBox = null;
    [SerializeField] private GameManager _gameManager = null;

    [Header("Deck Preview")]
    [SerializeField] private DeckBtn[] _deckBtn = null;

    private void Start()
    {
        for (int i = 0; i <_deckBtn.Length; i++)
        {
            if (i<Enum.GetValues(typeof(Deck.Pack)).Length)
            {
                _deckBtn[i].gameObject.SetActive(true);

                var id = i;
                _deckBtn[i].id = id;
                _deckBtn[i].GetComponent<Button>().onClick.AddListener(() => ChangeSelection(id));
            }
            else
                _deckBtn[i].gameObject.SetActive(false);
        }

        this.ResetSelectBox();
    }

    public void ResetSelectBox()
    {
        _gameManager.selectPack = _gameManager.currentPack;

        _selectBox.transform.position = _deckBtn[_gameManager.selectPack].transform.position;
        _gameManager.SetThemeColor((Deck.Pack)_gameManager.currentPack);
    }

    public void ChangeSelection(int id)
    {
        VSoundManager.Instance.Play(GameManager.sfx.UISelectFX);

        _selectBox.transform.position = _deckBtn[id].transform.position;
        _gameManager.selectPack = id;
        _gameManager.SetThemeColor((Deck.Pack)id);
    }
}
