﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class VPanel : MonoBehaviour
{
    public void OpenPopup()
    {
        this.gameObject.SetActive(true);

        this.transform.position = new Vector3(0f, 5f, 0f);

        this.transform.DOMoveY(0f, 0.35f).SetEase(Ease.OutCubic);
    }

    public void ClosePopup()
    {
        this.gameObject.SetActive(false);
    }
}
