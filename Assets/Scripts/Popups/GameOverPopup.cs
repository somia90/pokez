﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverPopup : VPanel
{
    [SerializeField] private TextMeshProUGUI _text = null;
    [SerializeField] private GameObject _ribbon = null;
    
    public void SetText(string str, bool record = false)
    {
        _text.text = str;
        _ribbon.SetActive(record);
    }
}
