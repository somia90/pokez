﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandListPopup : VPanel
{
    [SerializeField] private Deck _deck = null;
    [SerializeField] private Hand[] _hands = null;

    private List<int[]>[] _handRanking =
    {
        new List<int[]>(){ new int[] { 9, 3 }, new int[] { 10, 3 }, new int[] { 11, 3 }, new int[] { 12, 3 }, new int[] { 0, 3 }, },
        new List<int[]>(){ new int[] { 4, 0 }, new int[] { 5, 0 }, new int[] { 6, 0 }, new int[] { 7, 0 }, new int[] { 8, 0 }, },
        new List<int[]>(){ new int[] { 0, 3 }, new int[] { 0, 1 }, new int[] { 0, 2 }, new int[] { 0, 0 }, new int[] { 1, 3 }, },
        new List<int[]>(){ new int[] { 0, 0 }, new int[] { 0, 2 }, new int[] { 0, 1 }, new int[] { 12, 3 }, new int[] { 12, 0 }, },
        new List<int[]>(){ new int[] { 1, 3 }, new int[] { 3, 3 }, new int[] { 5, 3 }, new int[] { 7, 3 }, new int[] { 12, 3 }, },
        new List<int[]>(){ new int[] { 4, 3 }, new int[] { 5, 1 }, new int[] { 6, 2 }, new int[] { 7, 0 }, new int[] { 8, 3 }, },
        new List<int[]>(){ new int[] { 0, 1 }, new int[] { 0, 3 }, new int[] { 0, 0 }, new int[] { 1, 2 }, new int[] { 6, 1 }, },
        new List<int[]>(){ new int[] { 12, 2 }, new int[] { 12, 1 }, new int[] { 11, 3 }, new int[] { 11, 0 }, new int[] { 10, 2 }, },
        new List<int[]>(){ new int[] { 0, 1 }, new int[] { 0, 3 }, new int[] { 8, 0 }, new int[] { 7, 2 }, new int[] { 6, 1 }, }
    };

    private Sprite GetSprite(int[] card)
    {
        return _deck.CardSprite((Card.Rank)card[0], (Card.Suit)card[1]);
    }

    public void SetSprite()
    {
        for (int i = 0; i < _hands.Length; i++)
        {
            Sprite[] sprites = new Sprite[5];
            
            for (int j =0; j < 5; j++)
            {
                sprites[j] = this.GetSprite(_handRanking[i][j]);
            }

            _hands[i].SetSprite(sprites);
        }
    }
}
