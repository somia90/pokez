﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CardGroup : MonoBehaviour
{
    public enum GroupType
    {
        Single,
        Hor2,
        Ver2
    }

    [SerializeField] private GameObject[] _card = null;
    [SerializeField] private GameManager _gameManager;

    private bool _canDrag;
    public bool CanDrag
    {
        get { return _canDrag; }
        set 
        {
            _canDrag = value;
            if (!_canDrag)
            {
                _card[0].SetActive(false);
                _card[1].SetActive(false);
            }
        }
    }

    private Vector3 _initPos;
    public Vector3 InitPos { get { return _initPos; } }

    private GroupType _type;
    public GroupType Type { get { return _type; } set { _type = value; } }

    public bool allowInput;

    private List<Slot> _combo1;
    private List<Slot> _combo2;
    private List<Slot> _combo3;

    private void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        
        this.CanDrag = false;
        this.allowInput = true;

        _initPos = this.GetComponent<RectTransform>().position;

        _combo1 = new List<Slot>();
        _combo2 = new List<Slot>();
        _combo3 = new List<Slot>();
    }

    public void PrepareCardGroup(GroupType type)
    {
        this.Refresh(type);

        this.ResetCardGroup();
        this._type = type;
    }

    public void Refresh(GroupType type)
    {
        switch (type)
        {
            case GroupType.Single:
                _card[0].SetActive(true);
                _card[1].SetActive(false);

                this.GetComponent<GridLayoutGroup>().constraintCount = 1;
                break;
            case GroupType.Hor2:
                _card[0].SetActive(true);
                _card[1].SetActive(true);

                this.GetComponent<GridLayoutGroup>().constraintCount = 2;
                break;
            case GroupType.Ver2:
                _card[0].SetActive(true);
                _card[1].SetActive(true);

                this.GetComponent<GridLayoutGroup>().constraintCount = 1;
                break;
        }
    }

    private void OnMouseDrag()
    {
        if (this.CanDrag && !_gameManager.IsLose && allowInput)
        {
            Vector3 _dragPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
            this.transform.position = _dragPos + transform.up * 1.4f;

            _card[0].GetComponent<SpriteRenderer>().sortingLayerName = "Popup";
            _card[1].GetComponent<SpriteRenderer>().sortingLayerName = "Popup";
        }
    }

    private void OnMouseUp()
    {
        if (allowInput && !_gameManager.IsLose)
        {
            switch (this.Type)
            {
                case GroupType.Single:
                    this.CheckDropSingle();
                    break;
                case GroupType.Ver2:
                    this.CheckDropVer2();
                    break;
                case GroupType.Hor2:
                    this.CheckDropHor2();
                    break;
            }

            if (!_canDrag)
                this.PrepareNextMove();
            else
            {
                VSoundManager.Instance.Play(GameManager.sfx.ErrorMoveFX);
                this.ResetCardGroup();
                this.CardShake();
            }  

            _card[0].GetComponent<SpriteRenderer>().sortingLayerName = "Deck";
            _card[1].GetComponent<SpriteRenderer>().sortingLayerName = "Deck";
        }
    }

    private void CardShake()
    {
        this.transform.DOMove(this.InitPos, 0.15f).OnComplete(() =>
        {
            this.transform.DOMoveX(this.InitPos.x + 0.1f, 0.05f).OnComplete(() =>
            {
                this.transform.DOMoveX(this.InitPos.x - 0.1f, 0.05f).OnComplete(() =>
                {
                    this.transform.DOMove(this.InitPos, 0.05f);
                });
            });
        });
    }

    private void CheckDropSingle()
    {
        if (this.GetCard1().CanDrop)
        {
            int[] _cardData1 = { (int)this.GetCard1().rank, (int)this.GetCard1().suit };

            this.GetCard1().slot.SetCard(_cardData1);

            this.CanDrag = false;

            _gameManager.addPoint(Combo.ComboType.HighCard);

            for (int i = 0; i < 5; i++) {
                _combo1.Add(_gameManager.slot[(int)this.GetCard1().slot.Pos.x + i * 5]);
                _combo2.Add(_gameManager.slot[(int)this.GetCard1().slot.Pos.y * 5 + i]);
            }
        }
    }

    private void CheckDropVer2()
    {
        if (this.GetCard1().CanDrop && this.GetCard2().CanDrop) 
        {
            int[] _cardData1 = { (int)this.GetCard1().rank, (int)this.GetCard1().suit };
            int[] _cardData2 = { (int)this.GetCard2().rank, (int)this.GetCard2().suit };

            this.GetCard1().slot.SetCard(_cardData1);
            this.GetCard2().slot.SetCard(_cardData2);

            this.CanDrag = false;

            _gameManager.addPoint(Combo.ComboType.HighCard, 1);

            for (int i = 0; i < 5; i++)
            {
                _combo1.Add(_gameManager.slot[(int)this.GetCard1().slot.Pos.x + i * 5]);
                _combo2.Add(_gameManager.slot[(int)this.GetCard1().slot.Pos.y * 5 + i]);
                _combo3.Add(_gameManager.slot[(int)this.GetCard2().slot.Pos.y * 5 + i]);
            }
        }
    }

    private void CheckDropHor2()
    {
        if (this.GetCard1().CanDrop && this.GetCard2().CanDrop)
        {
            int[] _cardData1 = { (int)this.GetCard1().rank, (int)this.GetCard1().suit };
            int[] _cardData2 = { (int)this.GetCard2().rank, (int)this.GetCard2().suit };

            this.GetCard1().slot.SetCard(_cardData1);
            this.GetCard2().slot.SetCard(_cardData2);

            this.CanDrag = false;

            _gameManager.addPoint(Combo.ComboType.HighCard, 1);

            for (int i = 0; i < 5; i++)
            {
                _combo1.Add(_gameManager.slot[(int)this.GetCard1().slot.Pos.x + i * 5]);
                _combo2.Add(_gameManager.slot[(int)this.GetCard1().slot.Pos.y * 5 + i]);
                _combo3.Add(_gameManager.slot[(int)this.GetCard2().slot.Pos.x + i * 5]);
            }
        }
    }

    public void ResetCardGroup()
    {
        this.CanDrag = true;

        _card[0].GetComponent<Card>().CanDrop = false;
        _card[1].GetComponent<Card>().CanDrop = false;
    }

    private void PrepareNextMove()
    {
        this.transform.position = this.InitPos;
        VSoundManager.Instance.Play(GameManager.sfx.CardDealFX[UnityEngine.Random.Range(0, 3)]);

        if (_combo1.Count > 0) _gameManager.CheckCombo(_combo1);
        if (_combo2.Count > 0) _gameManager.CheckCombo(_combo2);
        if (_combo3.Count > 0) _gameManager.CheckCombo(_combo3);

        this.gameObject.SetActive(false);

        _gameManager.CheckSpawnCard();
        _gameManager.CheckRemoveCard();
        _gameManager.CheckLoseCondition();

        _combo1.Clear();
        _combo2.Clear();
        _combo3.Clear();

        _card[0].GetComponent<Card>().CanDrop = false;
        _card[1].GetComponent<Card>().CanDrop = false;

        if (_gameManager.tutorial)
        {
            _gameManager.CheckEndTut();
        }

        _gameManager.SaveData();
    }

    public Card GetCard1()
    {
        return _card[0].GetComponent<Card>();
    }

    public Card GetCard2()
    {
        return _card[1].GetComponent<Card>();
    }
}
