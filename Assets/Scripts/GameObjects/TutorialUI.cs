﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour
{
    [SerializeField] private GameObject _handSprite = null;
    [SerializeField] private GameObject _highlight = null;
    [SerializeField] private TextMeshPro _text = null;

    private Vector3[] _pos =
    {
        new Vector3(0.35f, 0.94f),
        new Vector3(-0.70f, 0.48f),
        new Vector3(-0.70f, -0.44f)
    };

    private int _step;

    private Tweener _moveTween = null;
    private Tweener[] _fadeTween = null;

    private void Start()
    {
        SpriteRenderer[] _highlightSprite = _highlight.GetComponentsInChildren<SpriteRenderer>();

        _fadeTween = new Tweener[2];
        this.FadeSprite(_fadeTween[0], _highlightSprite[0]);
        this.FadeSprite(_fadeTween[1], _highlightSprite[1]);
    }

    private void FadeSprite(Tweener _tween, SpriteRenderer _sprite, bool _fadeIn = true)
    {
        var newFade = _fadeIn ? 0.3f : 1f;

        _tween = _sprite.DOFade(newFade, 0.5f).OnComplete(() =>
        {
            this.FadeSprite(_tween, _sprite, !_fadeIn);
        });
    }

    private void MoveHand(Vector3 groupPos, bool toGroup = true)
    {
        var pos = toGroup ? groupPos : _pos[_step];

        _moveTween.Kill();

        _moveTween = _handSprite.transform.DOMove(pos, 1.5f).SetEase(Ease.InOutCubic).OnComplete(()=>
        {
            this.MoveHand(groupPos, !toGroup);
        });
    }

    public void SetUI(int step, Vector3 groupPos)
    {
        _step = step;
        _highlight.GetComponent<GridLayoutGroup>().constraintCount = _step == 0 ? 2 : 1;

        _highlight.transform.position = _pos[step];

        _handSprite.transform.position = _pos[step];
        this.MoveHand(groupPos);
    }

    public void ShowText(int textID)
    {
        switch (textID)
        {
            case 0:
                _text.text = "Place 2 similar cards next to each other to score a Pair";
                break;
            case 1:
                _text.text = "Fill a column/row with 5 cards to check Hand Ranking";
                break;
            case 2:
                _text.text = "Try higher rank Poker hands for more points";
                break;
            case 3:
                _step = 3;
                _moveTween.Kill();
                _handSprite.SetActive(false);
                _highlight.SetActive(false);
                _text.text = "You can check all available Poker hands in Option Panel";
                break;
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _step != 3)
        {
            _handSprite.SetActive(false);
            _highlight.SetActive(false);
        }

        if (Input.GetMouseButtonUp(0) && _step != 3)
        {
            _handSprite.SetActive(true);
            _highlight.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        _moveTween.Kill();

        _fadeTween[0].Kill();
        _fadeTween[1].Kill();
    }
}
