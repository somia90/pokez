﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveState
{
	public CardData[] deck;
	public CardData[] slot;
	public CardGroupData[] group;

	public int[] slotState;

	public bool lose;
    public bool ad;

    public int lv;
    public int combo;
    public int score;
    public int deal;
	public int turn;

	public SaveState()
	{
		slot = new CardData[25];
		group = new CardGroupData[3];

		slotState = new int[25];

		for (int i = 0; i < slot.Length; i++)
        {
			CardData c = new CardData();

			slot[i] = c;
        }

		for (int i = 0; i < group.Length; i++)
        {
			group[i] = new CardGroupData();

			group[i].c1 = new CardData();
			group[i].c2 = new CardData();
        }
	}
}

[System.Serializable]
public class CardData
{
	public int r = 0;
	public int s = 0;
}

[System.Serializable]
public class CardGroupData
{
	public CardData c1;
	public CardData c2;
	public bool drag = false;
	public int type = 0; 
}
