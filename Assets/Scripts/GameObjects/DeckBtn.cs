﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeckBtn : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _packNameText = null;

    private string _packName;
    private int _id;
    public int id
    {
        get { return _id; }
        set
        {
            _id = value;
            _packName = ((Deck.Pack)_id).ToString();
            _packNameText.text = _packName;

            string path = "Sprites/Cards/" + _packName + "_preview";

            this.GetComponent<Button>().image.sprite = Resources.Load<Sprite>(path);
        }
    }
}
