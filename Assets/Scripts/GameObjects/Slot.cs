﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour, ICard
{
    // To check if this slot is an empty slot, otherwise display card based on value and suit
    [SerializeField] private bool _isEmpty;
    public bool IsEmpty
    {
        get { return _isEmpty; }
        set { _isEmpty = value; }
    }

    // Properties and variables
    [SerializeField] private Vector2 _pos;
    public Vector2 Pos
    {
        get { return _pos; }
    }

    [SerializeField] private Card.Rank _rank;
    public Card.Rank rank
    {
        get { return _rank; }
        set { _rank = value; }
    }

    [SerializeField] private Card.Suit _suit;
    public Card.Suit suit
    {
        get { return _suit; }
        set { _suit = value; }
    }

    private Deck _deck;
    private SpriteRenderer _spriteRenderer;
    private Sprite _emptySprite;

    public float highlightColor = 0.7f;
    public float defaultColor = 0.8f;

    private void Awake()
    {
        // Since we have alot of Slots, we will let them find the Deck by themselves
        _deck = GameObject.FindObjectOfType<Deck>();

        // Init SpriteRenderer here for later use, since we will use SetSprite function alot
        _spriteRenderer = this.GetComponent<SpriteRenderer>();

        this.IsEmpty = true;

        // We will need to store the empty Slot sprite for later use
        _emptySprite = _spriteRenderer.sprite;
    }

    private void Start()
    {
        if(!_isEmpty)
            this.SetSprite(_deck.CardSprite(rank, suit));

        // Get slot index inside the table & assign is position
        int _index = this.transform.GetSiblingIndex();
        this._pos.x = _index % 5;
        this._pos.y = _index / 5;
    }

    // SetCard function store a Card's data & replace Slot with a Card sprite based on the data
    public void SetCard(int[] card)
    {
        rank = (Card.Rank)card[0];
        suit = (Card.Suit)card[1];
        this.IsEmpty = false;
        this.SetSprite(_deck.CardSprite(rank, suit));
    }

    // Display the assigned sprite on Slot
    public void SetSprite(Sprite sprite)
    {
        if (sprite && !_isEmpty)
        {
            _spriteRenderer.sprite = sprite;
            this.SetColor();
        }
    }

    // Return current card to become an empty Slot
    public int[] DisposeCard()
    {
        this.IsEmpty = true;
        this.Refresh();

        int[] _disposeCard = { (int)rank, (int)suit };
        return(_disposeCard);
    }

    public void Refresh()
    {
        this.IsEmpty = true;

        _spriteRenderer.sprite = _emptySprite;
        this.SetColor(defaultColor);
    }

    public void SetColor(float c = 1f)
    {
        this.GetComponent<SpriteRenderer>().color = new Color(c, c, c);
    }
}
