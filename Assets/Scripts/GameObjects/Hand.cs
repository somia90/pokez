﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hand : MonoBehaviour
{
    [SerializeField] private Image[] _cards = null;

    public void SetSprite(Sprite[] cards)
    {
        if (cards.Length == 5)
        {
            for (int i = 0; i < 5; i++)
            {
                _cards[i].sprite = cards[i];
            }
        }
    }
}
