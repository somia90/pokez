﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public interface ICard
{
    void SetSprite(Sprite sprite);
    Card.Rank rank { get; set; }
    Card.Suit suit { get; set; }
}

public class Card : MonoBehaviour, ICard
{
    public enum Suit
    {
        Spade,
        Club,
        Diamond,
        Heart
    };
    public enum Rank
    {
        Ace,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King
    };

    [SerializeField] private Card.Rank _rank;
    public Card.Rank rank
    {
        get { return _rank; }
        set { _rank = value; }
    }

    [SerializeField] private Card.Suit _suit;
    public Card.Suit suit
    {
        get { return _suit; }
        set { _suit = value; }
    }

    private bool _canDrop;
    public bool CanDrop
    {
        get { return _canDrop; }
        set { _canDrop = value; }
    }

    private SpriteRenderer _spriteRender;

    private Slot _slot;
    public Slot slot
    {
        get { return _slot; }
        set { _slot = value; }
    }

    private void Awake()
    {
        _spriteRender = this.GetComponent<SpriteRenderer>();
    }

    public void SetSprite(Sprite sprite)
    {
        if (sprite)
        {
            _spriteRender.sprite = sprite;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.GetComponent<Slot>())
        {
            this.slot = col.gameObject.GetComponent<Slot>();

            if (this.slot.IsEmpty)
            {
                this.CanDrop = true;
                this.slot.SetColor(slot.highlightColor);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.GetComponent<Slot>())
        {
            Slot s = col.gameObject.GetComponent<Slot>();
            if (s.IsEmpty)
            {
                s.SetColor(s.defaultColor);
            }
        }

        this.CanDrop = false;
    }

    public void Showcard(Sprite backCard, Sprite frontCard, bool flip = true)
    {
        if (flip)
        {
            float _currenctScale = transform.localScale.x;
            this.GetComponent<SpriteRenderer>().sprite = backCard;

            transform.DOScaleX(0f, 0.1f).SetDelay(0.4f + Random.Range(0f, 0.3f)).OnComplete(() =>
            {
                this.GetComponent<SpriteRenderer>().sprite = frontCard;
                transform.DOScaleX(_currenctScale, 0.1f);
            });
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = frontCard;
        }
    }
}
