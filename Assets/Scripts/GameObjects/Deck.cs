﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{ 
    public enum Pack
    {
        pokez,
        color,
        classic,
        royal,
        shiba,
        uno,
        line,
        egypt,
        gothic,
        modern,
        retro
    }

    [Header("Current Pack")]
    [SerializeField] private Pack _cardPackName = Pack.pokez;

    [Header("Game Objects")]
    [SerializeField] private CardAnim _cardAnim;
    [SerializeField] private Camera _camera = null;

    [Header("Packs BG Color")]
    [SerializeField] private Color _defaultBG = Color.black;
    [SerializeField] private Color _egyptBG = Color.black;
    [SerializeField] private Color _shibaBG = Color.black;
    [SerializeField] private Color _gothicBG = Color.black;
    [SerializeField] private Color _kradBG = Color.black;
    [SerializeField] private Color _modernBG = Color.black;
    [SerializeField] private Color _lineBG = Color.black;
    [SerializeField] private Color _royalBG = Color.black;
    [SerializeField] private Color _retroBG = Color.black;

    [Header("UIController")]
    [SerializeField] private UIController _uiController = null;

    private Sprite[] _cardSprite;
    private List<int[]> _remainCards;
    private CardData[] _currentData;

    private List<CardAnim> _cardAnimPool;
    public List<CardAnim> CardAnimPool
    {
        get { return _cardAnimPool; }
        set { _cardAnimPool = value; }
    }

    public int getCardCounter { get; set; }

    private void Awake()
    {
        _cardSprite = new Sprite[53];
        _remainCards = new List<int[]>();
        _cardAnimPool = new List<CardAnim>();

        this.UpdateCardPack(_cardPackName);
    }

    private void Start()
    {
        
    }

    public void PrepareDeck()
    {
        int i, j;
        _remainCards.Clear();

        for (i = 0; i < 4; i++)
        {
            for (j = 0; j < 13; j++)
            {
                int[] _card = { j, i };
                _remainCards.Add(_card);
            }
        }

        getCardCounter = 0;

        this.Shuffle();
    }

    public void LoadDeck(CardData[] data)
    {
        _remainCards.Clear();

        for (int i = 0; i < data.Length; i++)
        {
            int[] c = { data[i].r, data[i].s };

            _remainCards.Add(c);
        }
    }

    public int[] GetCardData()
    {
        int[] _cardData = _remainCards.PopAt(0);

        getCardCounter++;

        if (getCardCounter >= 52)
        {
            getCardCounter = 52 - _remainCards.Count;
            this.Shuffle();
        }

        _cardAnim = _cardAnimPool.Count > 0 ? _cardAnimPool.PopAt(0) : Instantiate(_cardAnim);
        _cardAnim.name = "CardAnim";

        _cardAnim.gameObject.SetActive(true);
        _cardAnim.transform.position = new Vector3(transform.position.x - 0.05f, transform.position.y + 0.15f);
        _cardAnim.IsFold = true;
        _cardAnim.DealCard();

        return _cardData;
    }

    public void ReturnCard(int[] card)
    {
        _remainCards.Add(card);
    }

    public void ComboAnim(float _x, float _y, int _face, int _suit)
    {
        _cardAnim = _cardAnimPool.Count > 0 ? _cardAnimPool.PopAt(0) : Instantiate(_cardAnim);
        _cardAnim.name = "CardAnim";

        _cardAnim.gameObject.SetActive(true);
        _cardAnim.transform.position = new Vector3(_x, _y);
        _cardAnim.SetCard(_face, _suit);

        _cardAnim.LiftCard();
    }

    public void Shuffle()
    {
        _remainCards.Shuffle();
    }

    public void UpdateCardPack(Pack pack){
        string path = "Sprites/Cards/" + pack.ToString() + "_all";

        _cardSprite = Resources.LoadAll<Sprite>(path);

        this.UpdateDeckSprite();
        this.SetThemeColor(pack);

        _camera.backgroundColor = this.GetThemeColor(pack);
    }

    public void SetThemeColor(Pack pack)
    {
        _uiController.SetPanelColor(this.GetThemeColor(pack));
    }

    public Color GetThemeColor(Pack pack)
    {
        switch (pack)
        {
            case Deck.Pack.classic:
                return _modernBG;
            case Deck.Pack.shiba:
                return _shibaBG;
            case Deck.Pack.egypt:
                return _egyptBG;
            case Deck.Pack.gothic:
                return _lineBG;
            case Deck.Pack.color:
                return _kradBG;
            case Deck.Pack.modern:
                return _lineBG;
            case Deck.Pack.line:
                return _retroBG;
            case Deck.Pack.royal:
                return _royalBG;
            case Deck.Pack.retro:
                return _retroBG;
            case Deck.Pack.uno:
                return _gothicBG;
            default:
                return _defaultBG;
        }
    }

    public Sprite CardSprite(Card.Rank face, Card.Suit suit){
        return _cardSprite[(int)suit * 13 + (int)face];
    }

    public Sprite CardBackSprite()
    {
        return _cardSprite[52];
    }

    private void UpdateDeckSprite(){
        this.GetComponent<SpriteRenderer>().sprite = _cardSprite[52];

        for(int i = 0; i < this.transform.childCount; i++) {
            this.transform.GetChild(i).gameObject.GetComponent<SpriteRenderer>().sprite = _cardSprite[52];
        }
    }

    public CardData[] CurrentDeck()
    {
        _currentData = new CardData[_remainCards.Count];

        for (int i = 0; i < _currentData.Length; i++)
        {
            _currentData[i] = new CardData();

            _currentData[i].r = _remainCards[i][0];
            _currentData[i].s = _remainCards[i][1];
        }

        return _currentData;
    }
}
