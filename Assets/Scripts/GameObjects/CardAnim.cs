﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class CardAnim : MonoBehaviour
{
    // Start is called before the first frame update
    private Deck _deck;

    private int _id;
    public int Id
    {
        get { return _id; }
        set { _id = value; }
    }

    [SerializeField] private bool _isFold;
    public bool IsFold
    {
        get { return _isFold; }
        set 
        { 
            _isFold = value; 
            if (_isFold)
            {
                this.GetComponent<SpriteRenderer>().sprite = _deck.CardBackSprite();
            }
        }
    }

    private float _tweenDuration = 0.1f;

    private void Awake()
    {
        _deck = GameObject.FindObjectOfType<Deck>();

        this.IsFold = true;
    }

    private void Start()
    {
        this.Id = 0;
        //this.LiftCard();
    }

    public void SetCard(int face, int suit)
    {
        IsFold = false;
        this.GetComponent<SpriteRenderer>().sprite = _deck.CardSprite((Card.Rank)face, (Card.Suit)suit);
    }

    public void Showcard(int face, int suit)
    {
        float _currenctScale = transform.localScale.x;

        transform.DOScaleX(0f, _tweenDuration).SetEase(Ease.InQuad).OnComplete(() =>
        {
            IsFold = false;
            this.GetComponent<SpriteRenderer>().sprite = _deck.CardSprite((Card.Rank)face, (Card.Suit)suit);
            transform.DOScaleX(_currenctScale, _tweenDuration).SetEase(Ease.OutQuad);
        });
    }

    public void FoldCard()
    {
        float _currenctScale = _deck.transform.localScale.x;

        transform.DOScaleX(0f, _tweenDuration).SetEase(Ease.InQuad).OnComplete(() =>
        {
            IsFold = true;
            transform.DOScaleX(_currenctScale, _tweenDuration).SetEase(Ease.OutQuad);
        });
    }

    public void LiftCard()
    {
        float _curX = transform.localPosition.x * 1.15f;
        float _curY = (transform.localPosition.y - 0.5f) * 1.15f + 0.5f;

        transform.DOScale(0.95f, _tweenDuration);
        transform.DOMoveX(_curX, _tweenDuration * 2);
        transform.DOMoveY(_curY, _tweenDuration * 2);

        Invoke("MoveToDeck", Random.Range(_tweenDuration * 5, _tweenDuration * 9));
    }

    public void MoveToDeck()
    {
        this.GetComponent<SpriteRenderer>().sortingOrder = 0;

        this.FoldCard();

        transform.DOScaleY(_deck.transform.localScale.y, _tweenDuration);
        transform.DOMove(_deck.transform.position, _tweenDuration * 6).SetEase(Ease.InOutQuad).OnComplete(() =>
        {
            this.gameObject.SetActive(false);
            _deck.CardAnimPool.Add(this);
        });
    }

    public void DealCard()
    {
        this.GetComponent<SpriteRenderer>().sortingOrder = 6;
        transform.DOMoveY(transform.position.y + 2f, _tweenDuration * 3).SetDelay(Random.Range(0f , 0.2f)).SetEase(Ease.InOutQuad).OnComplete(() =>
        {
            this.gameObject.SetActive(false);
            _deck.CardAnimPool.Add(this);
        });
    }
}
